#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import requests
import json
import config
import os
import os.path
import time
import sys

etc = config.etc(__file__)

if __name__ == '__main__':
    verbose = '-v' in sys.argv[1:]
    cfg = json.load(etc.open("settings.json"))
    upd_url = cfg["update_url"]
    if upd_url is None:
        print "No update URL configured, quitting..."
        sys.exit(1)
    upd_url = upd_url.rstrip("/") + "/"
    updated_count = 0
    for f in cfg["dbs"]:
        if verbose:
            print "Updating", f, "...",
        f_path = etc.pathFor(f)
        headers = {}
        if os.path.isfile(f_path):
            mtime = os.stat(f_path).st_mtime
            headers["If-Modified-Since"] = time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(mtime))
        resp = requests.get(upd_url + f, headers=headers)
        if resp.status_code == 304:
            if verbose:
                print "nothing new"
            continue
        resp.raise_for_status()
        if verbose:
            print "download complete!"
        with open(f_path + ".new", "wb") as fd:
            fd.write(resp.content)
        if os.path.isfile(f_path):
            os.rename(f_path, f_path + ".old")
        os.rename(f_path + ".new", f_path)
        if os.path.isfile(f_path + ".old"):
            os.unlink(f_path + ".old")
        os.chmod(f_path, 0644)
        updated_count += 1
    if updated_count:
        if verbose:
            print "Sending reload message..."
        etc.sendReloadConfig(True)
