#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, request, render_template, redirect, url_for
import os.path
import sys
import config

etc = config.etc(__file__)

app = Flask(__name__)

base_ctx = {
        'menu_items': {
            'Status': '/status',
            'Custom blacklist': '/customBlackList',
            #'Automatic blacklist': '/automaticBlackList'
            }
        }

@app.route('/')
def root():
    return redirect(url_for('statusPage'))

@app.route('/status')
def statusPage():
    logs = etc.callLogs()
    log_file = request.args.get("log_file")
    if log_file not in logs:
        log_file = None
    if log_file is None and len(logs):
        log_file = logs[0]
    calls = []
    prev_log = None
    next_log = None
    if log_file:
        calls = etc.parseCallsLog(log_file)
        lf_idx = -1
        try:
            lf_idx = logs.index(log_file)
        except:
            pass
        if lf_idx > 0:
            prev_log = logs[lf_idx-1]
        if lf_idx < len(logs)-1:
            next_log = logs[lf_idx+1]

    ctx = {}
    ctx.update(base_ctx)
    ctx.update({
            "calls": calls,
            "logs": logs,
            "prev_log": prev_log,
            "selected_log": log_file,
            "next_log": next_log,
            })
    ctx.update(base_ctx)
    return render_template('status.html', **ctx)

@app.route('/customBlackList', methods=['GET', 'POST'])
def customBlackListPage():
    if request.method == 'POST':
        blacklist = request.form["blacklist"]
        whitelist = request.form["whitelist"]
        etc.open("blacklist", "w").write(blacklist)
        etc.open("whitelist", "w").write(whitelist)
        etc.sendReloadConfig()
    ctx = {}
    ctx.update(base_ctx)
    ctx.update({
            'blacklist_text': etc.open("blacklist", "r").read(),
            'whitelist_text': etc.open("whitelist", "r").read()
            })
    return render_template('custom_blacklist.html', **ctx)

@app.route('/automaticBlackList')
def automaticBlackListPage():
    return statusPage()

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
