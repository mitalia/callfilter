#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import logging
import logging.handlers
import sys
import pexpect.fdpexpect
import os
import re
import time
import mtzf
import config
import json
import signal

etc = config.etc(__file__)

FORMAT = "%(asctime)s %(name)s %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)
calls_log = logging.getLogger("calls_log")
logfile = logging.handlers.TimedRotatingFileHandler(
            etc.pathFor("modem_master.log"), when='D'
            )
logfile.setFormatter(logging.Formatter(FORMAT.replace(" ", chr(0x1e))))
logging.getLogger().addHandler(logfile)

cfg = {}
DBS = []
BLACKLIST = []
WHITELIST = []
DEFAULT_PREFIX = ""
db = None

def reload_parms():
    global cfg, DBS, BLACKLIST, WHITELIST, DEFAULT_PREFIX
    cfg = json.load(etc.open("settings.json"))
    DBS = [etc.pathFor(f) for f in cfg["dbs"]]
    BLACKLIST = etc.parseBWList("blacklist")
    WHITELIST = etc.parseBWList("whitelist")
    DEFAULT_PREFIX = cfg.get("default_prefix")
    logging.info("Parameters reloaded!")

def load_dbs():
    global db
    # load the MTZF DB
    logging.info("Loading db...")
    logging.info(DBS[0])
    with open(DBS[0], "rb") as fp:
        db = mtzf.RatingsDB(fp)
    for d in DBS[1:]:
        logging.info(d)
        with open(d, "rb") as fp:
            db2 = mtzf.RatingsDB(fp)
            db.merge_update(db2)
    logging.info("DB loaded! %d entries loaded" % (len(db.entries),))

def reload_all():
    reload_parms()
    load_dbs()

etc.open("pid", "w").write("%d\n" % os.getpid())
reload_parms()
PORT = cfg["port"]
mdm = None

# reload most parameters on SIGHUP
signal.signal(signal.SIGHUP, lambda num, frame: reload_parms())
signal.signal(signal.SIGUSR1, lambda num, frame: reload_all())
try:
    load_dbs()
    # open the modem
    fd = os.open(PORT, os.O_RDWR|os.O_NONBLOCK|os.O_NOCTTY)
    mdm = pexpect.fdpexpect.fdspawn(fd)

    non_num = re.compile(r"[^\d]")

    # sends s (if any), expects an OK (goes on) or an ERROR (raises exception)
    def expectok(s=None):
        if s is not None:
            mdm.send(s)
        if mdm.expect(["OK\n", "ERROR\n"])==1:
            raise RuntimeError("Got ERROR waiting for OK", s)

    # do a number match against the various sources
    def matchnumber(number):
        # explicit whitelist
        if number in WHITELIST:
            return (False, "whitelist")
        # explicit blacklist
        if number in BLACKLIST:
            logging.info("Found %s in blacklist", number)
            return (True, "blacklist")
        if number == '?':
            # don't try a lookup for unknown caller, it raises an exception
            return (False, "unknown allowed")
        # lookup the number
        entry = db.lookup(number)
        if entry is None:
            return (False, "not found in db")
        logging.info("Found %s in DB: %s", number, repr(entry))
        tot_votes = entry.good_votes + entry.bad_votes + entry.neutral_votes
        # ignore small sample size
        if tot_votes < 2:
            logging.info("Entry has too few votes (%d)", tot_votes)
            return (False, "inconclusive db")
        # require at least 50% of bad votes
        if entry.bad_votes / float(tot_votes) < 0.5:
            return (False, "good for db")
        return (True, "bad for db")

    # throw away leftovers
    mdm.flush()
    # reality check - is this a modem?
    mdm.send("AT\r")
    # notice that here we may get either an ECHO-ed response or a "normal" one,
    # depending from how was the modem left before we started
    mdm.expect(["AT\n\n\nOK\n\n", "OK\n"])
    # disable ECHO
    mdm.send("ATE0\r")
    mdm.expect(["ATE0\n\n\nOK\n\n", "OK\n"])
    # enable caller ID
    # try with some common strings
    logging.info("Enabling caller id...")
    for s in ["AT#CID=1\r", "AT+VCID=1\r", "AT#CC1\r", "AT*ID1\r", "AT%CCID=1\r", "AT#CLS=8#CID=1\r"]:
        mdm.send(s)
        if mdm.expect(["OK\n", "ERROR\n"])==0:
            logging.info("Success! The modem liked %r", s)
            break
    else:
        raise RuntimeError("No caller ID enable command worked")

    while True:
        # keep the modem on its toes
        expectok("AT\r")
        # wait for a NMBR
        if mdm.expect(["NMBR *= *([^\\n]*)\n", pexpect.TIMEOUT]) == 1:
            continue
        # kill non-numeric data (never seen, but who knows)
        number = non_num.sub("", mdm.match.group(1))
        logging.info("Got call from %s => %s", repr(mdm.match.group(1)), number)
        if len(number)==0:
            logging.info("Unknown caller")
            number = "?"
        # match both with and without the default international prefix
        bad = False
        if number != "?":
            bad,msg = matchnumber(DEFAULT_PREFIX + number)
        if not bad:
            bad,msg = matchnumber(number)
        # create the delimited message for the web GUI
        calls_log.info("%s\x1e%s (%s)" % (number, "✘" if bad else "✔", msg))
        if not bad:
            logging.info("Call is good")
            # wait for the rings to finish
            while mdm.expect(["RING\n", pexpect.TIMEOUT], timeout = 8) == 0:
                pass
            continue
        # bad call
        logging.info("Call is bad")
        # wait for an actual RING, otherwise some modems ignore the ATA
        if mdm.expect(["RING\n", pexpect.TIMEOUT]) == 1:
            continue
        # pick up the call, wait a moment and hang up
        mdm.send("ATA\r")
        time.sleep(0.5)
        # anything will stop the modem from trying to communicate modem-wise
        mdm.send(" ")
        mdm.expect(["NO CARRIER\n", "ERROR\n"])
        # hangup
        expectok("ATH\r")
except:
    logging.exception("Got exception =(")
finally:
    if mdm:
        # be a good boy and clean up
        mdm.send("ATH\r")
        mdm.expect("OK\n")
        mdm.close()
    os.unlink(etc.pathFor("pid"))
