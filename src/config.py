import os.path
import sys
import glob
import re
import os
import signal

non_numbers_re = re.compile("[^0-9]")

class etc:
    def __init__(self, dfile=__file__):
        self.confDir =  os.path.join(os.path.dirname(os.path.realpath(dfile)), '..', 'etc')
        if len(sys.argv)>1:
            confDir = sys.argv[1]

    def open(self, name, *args, **kwargs):
        return open(self.pathFor(name), *args, **kwargs)

    def pathFor(self, name):
        return os.path.join(self.confDir, name)

    def callLogs(self):
        ret = sorted([os.path.basename(f) for f in glob.glob(self.pathFor("modem_master.log*"))], reverse=True)
        if len(ret):
            # the current log goes on top
            ret = ret[-1:] + ret[:-1]
        return ret

    def parseCallsLog(self, name):
        ret = []
        with self.open(name) as fd:
            for l in fd:
                l = l.strip('\n').decode('utf8')
                v = l.split(chr(0x1e))
                if len(v) < 3:
                    continue
                if v[1]!="calls_log":
                    continue
                v = [v[0]] + v[2:]
                ret.append(tuple(v))
        return ret

    def parseBWList(self, name):
        ret = set()
        with self.open(name) as fd:
            for l in fd:
                non_comment = l.split("#", 1)[0].strip()
                if non_comment == '?':
                    ret.add(non_comment)
                else:
                    number = non_numbers_re.sub("", non_comment)
                    if len(number):
                        ret.add(number)
        return ret

    def sendReloadConfig(self, reloadDBs = False):
        try:
            target_pid = int(self.open("pid", "r").read())
        except IOError:
            return False
        os.kill(target_pid, signal.SIGUSR1 if reloadDBs else signal.SIGHUP)
        return True
