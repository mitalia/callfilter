import sys
import struct
import re

categories = {
        1: "telemarketer",
        2: "debt collector",
        3: "silent call",
        4: "nuisance call",
        5: "unsolicited call",
        6: "call center",
        7: "fax machine",
        8: "non-profit org",
        9: "political call",
        10: "scam call",
        11: "prank call",
        12: "sms",
        13: "survey",
        14: "other",
        15: "finance service",
        16: "company",
        17: "service"
        }

class RatingsDB:

    class EntryData:
        def __init__(self):
            self.good_votes = 0
            self.bad_votes = 0
            self.neutral_votes = 0
            self.reserved = 0
            self.category = 0

        def __repr__(self):
            return repr(self.__dict__)

        def prettyprint(self):
            return "good: %d\nneutral: %d\nbad: %d\ncategory: %s" % (
                    self.good_votes, self.neutral_votes, self.bad_votes,
                    categories.get(self.category, "unknown (%d)" % (self.category,))
                    )

    def __init__(self, fp):
        def ru(fmt):
            fmt = "<" + fmt
            data = fp.read(struct.calcsize(fmt))
            return struct.unpack(fmt, data)

        self.non_decimal = re.compile("[^\d]+")
        self.entries = {}
        magic = fp.read(4).upper()
        if magic == "MTZF":
            self.full = True
        elif magic == "MTZD":
            self.full = False
        else:
            raise RuntimeError("Invalid header")
        self.format_version, = ru("B")
        if self.format_version != 1:
            raise RuntimeError("Unsupported format version", self.format_version)
        self.data_version, = ru("I")
        self.data_country = fp.read(2)
        self.reserved, = ru("i")
        if self.reserved != 0:
            sys.stderr.write("Warning: nonzero reserved byte\n")
        record_count, = ru("I")
        for i in xrange(record_count):
            e = RatingsDB.EntryData()
            number,e.good_votes,e.bad_votes,e.neutral_votes,e.reserved,e.category = ru("QBBBBB")
            self.entries[number] = e
        if fp.read(2).upper() != "CP":
            raise RuntimeError("Missing CP section")
        record_count, = ru("I")
        self.to_delete = []
        for i in xrange(record_count):
            self.to_delete.append(ru("Q")[0])
        if fp.read(6).upper() != "MTZEND":
            raise RuntimeError("Missing endmark")

    def lookup(self, number):
        n = int(self.non_decimal.sub("", number))
        return self.entries.get(n, None)

    def merge_update(self, other):
        if not self.full:
            raise RuntimeError("Can only merge from full DBs")
        self.entries.update(other.entries)
        for n in other.to_delete:
            if n in self.entries:
                del self.entries[n]

if __name__=='__main__':
    db = None
    dbs = sys.argv[1:]
    print "Loading db %s..." % (dbs[0])
    with open(dbs[0], "rb") as fp:
        db = RatingsDB(fp)
    for d in dbs[1:]:
        with open(d, "rb") as fp:
            print "Loading extra db %s..." % (d)
            db2 = RatingsDB(fp)
            db.merge_update(db2)
    print "Done!", len(db.entries), "entries loaded!"
    while True:
        entry = db.lookup(raw_input())
        if entry is None:
            print "Not found"
        else:
            print entry.prettyprint() + "\n"

